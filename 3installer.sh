# system updates+upgrades
apt-get update && apt-get -y upgrade
# install nano (editor)
apt-get install -y build-essential nano
# get source file
wget --no-check-certificate https://bitbucket.org/fl3x_/3proxy-int/raw/0497c38487ff1014113515ada0d22b9f5c5cf78b/3proxy-3proxy-0.8.6.tar.gz
# untar sourcefile
tar xzf 3proxy-0.8.6.tar.gz
# enter folder
cd 3proxy-3proxy-0.8.6
# let the magic happen
make -f Makefile.Linux
# enter folder
cd src
# create 3proxy folder
mkdir /etc/3proxy/
# move sourcefolder
mv 3proxy /etc/3proxy/
# enter folder
cd /etc/3proxy/
# get config file
wget --no-check-certificate https://github.com/barankilic/3proxy/raw/master/3proxy.cfg
# set permissions
chmod 600 /etc/3proxy/3proxy.cfg
# get authfile
wget --no-check-certificate https://github.com/barankilic/3proxy/raw/master/.proxyauth
# set permissions
chmod 600 /etc/3proxy/.proxyauth
# get init script
wget --no-check-certificate https://raw.github.com/barankilic/3proxy/master/3proxyinit
# make it executable
chmod  +x /etc/init.d/3proxyinit
# add 3proxy to autostart
update-rc.d 3proxyinit defaults
