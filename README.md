* # *3proxy installer Script* #
* **Attention: **This Script is still work in progress!
* This script is a part of the iWant-Scripts series
* Actually it only works for Debian based VPS
###  ###
* # *Description* #
* A simple script to install the russian proxy software named 3proxy.
* 3proxy is as an elite high anonymous proxy with SSL support. 
* Software Link: [Go to page](https://3proxy.ru/)
###  ###
* # *Script is developed for* #
* Debian 6 32/64bits
* Debian 7 32/64bits
* Debian 8 32/64bits
###  ###
* # *Do you like iWant-Scripts series?* #
* Send us a virtual beer (BTC only): **1Myb2mtNXUH7VQvVzfxWB6SWJ4JABtnBSs**